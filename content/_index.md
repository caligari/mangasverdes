---
type: page
---

| Título | Mangas Verdes |
|:---------:|:----------|
| Resumen | Solución para hospitales de campaña u hoteles medicalizados para solicitar atención desde paciente en cama a un control de enfermería centralizado en planta. |
| Distribución | Este documento y sus actualizaciones estarán públicamente disponibles con [licencia CC0][LICEN01] (pública sin ninguna restricción) en https://mangasverdes.makedevelop.com Por favor, hagan referencia a esta dirección si distribuyen el documento. |
| Contacto | Para cualquier sugerencia sobre este documento puede contactar con mangasverdes@makedevelop.com Agradecemos _feedback_ o que se nos comunique cualquier implementación para publicarlas en beneficio de todos. |
| Versiones | #1 (24 mar 2020) Idea y objetivos. |
|           | #2 (25 mar 2020) Dispositivo avisador de cama. |
  
[LICEN01]: https://choosealicense.com/licenses/cc0-1.0/

## Introducción

Ante la situación de **emergencia por el _Covid-19_** y amplicación de hospitales a **hoteles medicalizados y hospitales de campaña** surge la necesidad de proveer soluciones para estos improvisados escenarios de atención directa de los pacientes en camas.

En este documento se desarrolla una posible implementación para dar servicio de **comunicación desde el paciente en la cama al control de enfermería** que atiende a un conjunto de camas en planta.

**_MakeDevelop_**, como empresa parte del [_Grupo Inverdama_][INVER01] con base en Galicia y delegaciones por España y el mundo, ha pensado en la solución descrita a continuación teniendo en cuenta que tiene un producto de automatización y control de instalaciones industriales (descrito en el apartado "Adquio para control de enfermería").

Sin embargo, aunque la propuesta está basada en el producto _Adquio_ de _MakeDevelop_ como producto desarrollado y funcional, este documento pretende que también sirva para ilustrar la solución de atención en camas, a la vez que pueda ser de inspiración o **guía para otras propuestas alternativas y adaptables** a cada caso de necesidad que se produzca en cualquier lugar y momento.

Por ello, se describen **sistemas sencillos o alternativos, fáciles de montar y construir**, sin más requerimientos que la premura de tener la hospitalización de emergencia funcionando eficazmente en el menor tiempo posible.

Sin embargo, queremos advertir que es **importante que cualquier desarrollo relacionado con la salud debe ser previamente auditado a su aplicación por especialistas en medicina y enfermería** y, se debe hacer en colaboración en primera instancia desde los requisitos que impongan desde su criterio.

En base al caso del despliegue del hospital de campaña en IFEMA de Madrid y la medicalización de algunos hoteles, nos hemos puesto en contacto con personas relacionadas con atención clínica y gestión de recursos en hospitales de Galicia.

Las **primeras impresiones han sido positivas** con respecto a la idea, especialmente en el caso de los hoteles medicalizados, ya que los pacientes estarían aislados en la habitación, no hay visibilidad directa y no es viable estar entrando en ellas para comprobar si el paciente necesita atención.

[INVER01]: http://www.grupoinverdama.com

## Requisitos del problema

En el escenario de una hospitalización con pacientes distribuidos en planta y un control de enfermería que centraliza su atención clínica:

* Es necesario un sistema para que un paciente consciente en cama pueda llamar la atención del personal de enfermería y se pueda **localizar y acudir a la cama** en cuestión.
* En la parte de la cama habrá un **accionador siempre accesible** por el paciente y que usará para reclamar la atención de enfermería.
* En el control de enfermería se notificará qué cama requiere de atención indicando su posición en un **mapa de la planta con camas**.
* En el mapa se alertará con **indicaciones visuales y acústicas** (desactivables o ajustables en volumen) desde que se recibe la notificación desde el paciente.

## Dispositivo avisador en cama (opción básica)

La finalidad de este dispositivo es detectar cuando se activa el pulsador por el paciente y comunicarse con otro dispositivo remoto en el control de enfermería para notificar la solicitud de atención.

Funcionalmente se compone de:

- Activador (normalmente un botón pulsador accionable con la mano)
- Indicador de envío de notificación (normalmente un LED)
- Indicador de posición de la cama (puede ser el mismo LED que el anterior)

### Implementación con shield "MangasVerdes" para Arduino

Se ha diseñado esta shield para conectar a un Arduino Uno y que sirva de interfaz por protocolo Modbus al controlador en la central de enfermería y gestiona activador con un pulsador e indicador luminoso con LED.

![Shield "mangasverdes" en Arduino](shield_mangasverdes_arduino.jpg)

El montaje básico propuesto para una unidad por cada cama incluye los siguientes elementos:

- 1 x Shield Arduino "MangasVerdes" (diseño inicial para hacer placa en CNC)
- 3 x Hilos de cobre para conectar cada dispositivo con el de la cama anterior (valen los de ethernet UTP)
- 1 x Arduino UNO con cable USB (suele venir con el Arduino para conectar al ordenador)
- 1 x Cargador de corriente 125/220V a USB (típico de los teléfonos móviles)
- 1 x Pera-pulsador con cable de par de hilos 80-100cm
- 1 x Mástil de 150-180cm (tubo PVC ó palo de madera)
- 1 x Brida ~15cm para fijar Arduino al mástil
- 2 x Brida ~30cm para fijar mástil a cama (dependiendo de cama)

El **mástil** debe ser suficientemente rígido para el peso y es necesario fijarlo en vertical a la cama en alguna posición del tercio de cama que parte de la cabeza y dependerá de la estructura de ésta para poder hacerlo. Es previsible que ese mástil se puede usar, o ya exista, para algún soporte de gotero o lámpara de cama.

Las bridas se proponen de un largo aproximado pero depende de la distancia que haya entre el mástil y la estructura rígida de la cama en cada caso (somier, largueros, patas...).

El Arduino con la shield en la parte superior del mástil tienen 2 funciones: acortar la distancia del cable que va a la pera-pulsador y hacer que el LED sea visible cuando esté parpadeando para localizar la cama fácilmente a lo lejos.

Se parte de que en el escenario habrá **alimentación en cada cama** para su uso con alguna lámpara que es muy necesaria para los turnos de noche. En caso de hoteles medicalizados no habrá problema pero, en los hospitales de campaña es necesario llevar cableado específico desde una toma cercana para proveer de energía de 220V donde conectar el cargador de móvil que servirá para alimentar la electrónica.

La pera-pulsador debiera ser **pulsador y no interruptor** conmutador. Si sólo se dispusiera de conmutador, hay que indicar al paciente ese funcionamiento o acordarse de ponerlo en posición apagado después de la atención al paciente. En esta foto se muestra un ejemplo de pera-pulsador fabricado con un pulsador comercial y un bote de muestras que abundan en los hospitales (cortesía de un miembro de la _asociación maker Bricolabs_):

![Ejemplo de pera-pulsador DiY](pera_pulsador_diy.jpg)

La shield debe ser configurada con una **dirección de dispositivo Modbus** que la identificará en la red. Esto se hace físicamente cortando las pistas indicadas en el [documento de manual técnico de la shield "mangas-verdes"][MVMAN01] ó programándolo directamente en el firmware (también indicado en el manual técnico).

La última versión del **firmware desarrollado** para la shield se publica en [este repositorio][MVREP01] está disponible con su código fuente para poder descargarlo al un PC estándar y grabarlo en la placa Arduino Uno instalando previamente el Software Libre de Arduino.

La parte de la configuración Modbus en el **controlador central** se detalla en la sección "Adquio para control de enfermería". En cuanto a la parte técnica del dispositivo en cama se simplifica a una variable que representa si la solicitud de atención está activa.

### Alternativas y ampliaciones

El uso de **Open Hardware** tan extendido como Arduino facilita que su construcción sea simple con pocos recursos al alcance de mucha gente, especialmente los que están en contacto con el mundo Maker. Por ello se ha decidido en el primer prototipo utilizar esta plataforma que permitiría que los dispositivos para camas sean sencillos de montar y mantener.

El uso de **Modbus RTU por cable** es una solución muy probada y segura a la hora de ponerla en funcionamento con la parte del controlador Adquio que se propone para la enfermería. Se evita el uso de tecnologías inalámbricas que aunque son sencillas y baratas de desplegar presentan más problemas de ruido y colapso de comunicaciones. Aún así, una comunicación inalámbrica para espacios reducidos podría ser viable (a desarrollar como alternativa).

En la versión más básica, la variable en cada dispositivo de cama indicando la solicititud de atención es suficiente para cubrir los requisitos. Es posible ampliar el desarrollo para manejar otras variables relacionadas con otra funcionalidad, pero téngase en cuenta que se busca un **sistema simple y eficaz**.

La construcción de la shield "mangas verdes" se propone inicialmente con el prototipo en formato de **PCB de una cara y con componentes THT** (fáciles de soldar a mano) para poder fabricarla rápidamente en centros maker que dispongan de máquinas CNC. Es previsible que se pueda fabricar en formato industrial si fuera necesario tenerlas en grandes cantidades y rápidamente.

![Shield "MangasVerdes" hecha con CNC](pcb_mangas_verdes.jpg)

Es interesante anotar como ampliación de requisitos una funcionalidad de **interfono** que nos ha comentado una enfermera. Consiste en comunicarse por voz con el paciente para evitar desplazarse a las camas innecesariamente o acudir allí con lo que fuera necesario para atender al paciente. Por modbus no es posible implementar capacidades de transmisión de voz en línea, se debe pensar en otros sistemas para esta funcionalidad ampliada.

[MVMAN01]: #pendiente
[MVREP01]: #pendiente

## Adquio para control de enfermería

### Comunicación a camas

Pendiente...

### Pantalla táctil con control de alertas

Pendiente...

### Alternativas y ampliaciones

Pendiente...


----
-Fin del documento-
